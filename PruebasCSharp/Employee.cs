using System;

namespace PruebasCSharp
{
	public class Employee
	{
		public Employee ()
		{
		}
		#region Properties
		public int Numero {
			get;
			set;
		}

		public DateTime FechaNacimiento {
			get;
			set;
		}

		public string Nombre {
			get;
			set;
		}

		public string Apellidos {
			get;
			set;
		}

		public Gender Genero {
			get;
			set;
		}

		public DateTime FechaContratacion {
			get;
			set;
		}
		#endregion
	}
}

