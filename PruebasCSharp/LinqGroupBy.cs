using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Data;
using System.Linq;
using AutoMapper;
using System.Data.Common;

namespace PruebasCSharp
{
	public class LinqGroupBy
	{
		List<Employee> employees;

		public LinqGroupBy ()
		{
			this.initAutoMapper ();
			this.init ();
		}

		public void ShowTheGroup ()
		{

			Console.WriteLine (this.employees.Count);

			this.employees.GroupBy (e => e.FechaNacimiento.Month)
				.Select (g => new {
					Mes = g.Key,
					Cantidad = g.Count ()
				})
					.OrderBy (e => e.Mes)
				.ToList ()
				.ForEach (e => {
				Console.WriteLine (string.Format ("Mes: {0} Cantidad: {1}", e.Mes, e.Cantidad));
			});

//			var result = this.employees.GroupBy (e => e.Genero)
//				.Select (g => new {
//					Genero = g.Key,
//					Cantidad = g.Count ()
//				}).ToList ();
//
//			result.ForEach (r => {
//				Console.WriteLine (string.Format ("Genero: {0} Cantidad: {1}", r.Genero, r.Cantidad));
//			});

		}

		private void init ()
		{
			var myConnectionString = "server=127.0.0.1;uid=root;" +
				"pwd=;database=employees;";

			try {
				var cnn = new MySqlConnection (myConnectionString);

				cnn.Open ();

				var command = new MySqlCommand ();

				command.CommandText = "select * from employees limit 100000";
				command.Connection = cnn;
				command.CommandType = CommandType.Text;

				var reader = command.ExecuteReader ();

				this.employees = new List<Employee> ();

				while (reader.Read()) {
					this.employees.Add(Mapper.Map<Employee>(reader));
				}

//				while (reader.Read()) {
//					this.employees.Add (new Employee {
//						Apellidos = reader["last_name"].ToString(),
//						FechaContratacion = Convert.ToDateTime(reader["hire_date"]),
//						FechaNacimiento = Convert.ToDateTime(reader["birth_date"]),
//						Genero = reader["gender"].ToString().Equals("M") ? Gender.MALE : Gender.FEMALE,
//						Nombre = reader["first_name"].ToString(),
//						Numero = Convert.ToInt32( reader["emp_no"])
//					});
//				}

			} catch (MySqlException ex) {
				Console.WriteLine (string.Format ("Error número: {0}", ex.Number));
			}
		}

		private void initAutoMapper ()
		{
			Mapper.Initialize (cfg => {
				cfg.CreateMap<MySqlDataReader, Employee> ()
					.ForMember (d => d.Apellidos, o => o.MapFrom (s => s.GetString("last_name")))
						.ForMember (d => d.FechaContratacion, o => o.MapFrom (s => s.GetDateTime("hire_date")))
						.ForMember (dest => dest.FechaNacimiento, opt => opt.MapFrom (src => src.GetDateTime ("birth_date")))
						.ForMember (dest => dest.Genero, opt => opt.MapFrom (src => src.GetString ("gender").Equals ("M") ? Gender.MALE : Gender.FEMALE))
						.ForMember (dest => dest.Nombre, opt => opt.MapFrom (src => src.GetString ("first_name")))
						.ForMember (dest => dest.Numero, opt => opt.MapFrom (src => src.GetInt32 ("emp_no")));
			});
			

		}
	}
}

